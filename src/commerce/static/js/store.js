let csrftoken = document.cookie.replace(
  /(?:(?:^|.*;\s*)csrftoken\s*\=\s*([^;]*).*$)|^.*$/,
  "$1"
);
let orderItems = [];
let total = 0;

let productOrder = (product) => {
  let item = `
        <div class="row align-items-start">
            <div class="col">
                <img src="${product.image}" style="width:150px">
            </div>
            <div class="col">
                <p><strong>${product.name}</strong></p>
                <p>${product.description}</p>
            </div>
            <div class="col">
                <p><strong>Cantidad:</strong> ${product.units}</p>
            </div>
            <div class="col">
                <p><strong>Precio Unitario:</strong> ${product.price}</p>
                <p><strong>Precio Total:</strong>  ${
                  product.price * product.units
                }</p>
            </div>
        </div>
    `;
  return item;
};

let getProductByID = (id, quantity) => {
  return new Promise((resolve) => {
    let found = true;
    for (var i = 0; i < orderItems.length; i++) {
      if (orderItems[i].id === parseInt(id)) {
        orderItems[i].units = quantity;
        found = false;
      }
    }
    if (found) {
      fetch(`/api/products/${id}/`)
        .then((resp) => resp.json())
        .then(function (data) {
          data.units = quantity;
          orderItems.push(data);
          let target = document.getElementById("orderitems");
          target.innerHTML = orderItems.length;
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  });
};

async function buildOrderList() {
  let target = document.getElementById("orderlist");
  if (orderItems.length > 0) {
    let items = "";
    for (var i = 0; i < orderItems.length; i++) {
      total += orderItems[i].price * orderItems[i].units;
      items += productOrder(orderItems[i]);
    }
    items += `
      <div class="modal-footer">
        <h4><strong>Total:</strong> ${total}</h4>
      </div>
      `;
    target.innerHTML = items;
  } else {
    target.innerHTML = `
      <div class="alert alert-primary" role="alert">
        Tú carrito esta vacío!
        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Continuar comprando</button>
      </div>
    `;
  }
}

async function addItem(item) {
  let select = document.getElementById("select" + item);
  var value = select.options[select.selectedIndex].value;
  document.getElementById("saveorder").removeAttribute("disabled");
  document.getElementById("pay").removeAttribute("disabled");
  document.getElementById("partialpayment").removeAttribute("disabled");
  await getProductByID(item, parseInt(value));
}

let quantitySelect = (quantity) => {
  let options = "";
  for (var i = 2; i <= quantity; i++) {
    options += `<option value="${i}">${i}</option>`;
  }
  return options;
};

let productItem = (product) => {
  let item = `
    <article>
        <img src="${product.image}" alt="Sample photo">
        <div class="text">
            <h4>${product.name}</h4>
            <p>${product.description}</p>
            <p><strong>Unidades disponibles:</strong> ${product.quantity}</p>
            <h5>$ ${product.price}</h5>
            <select class="form-select mt-2" aria-label="select" id="select${
              product.id
            }">
                <option selected value="1">1</option>
                ${quantitySelect(product.quantity)}
            </select>            
            <div class="d-flex justify-content-end mt-3">
                <a data-bs-item="${
                  product.id
                }" class="btn btn-primary btn-block additem">Agregar al carrito</a>
            </div>
        </div>
    </article>
    `;
  return item;
};

let buildProductList = (products) => {
  let productList = "";
  for (var i = 0; i < products.length; i++) {
    productList += productItem(products[i]);
  }
  let target = document.getElementById("products");
  target.innerHTML = productList;
  document.querySelectorAll(".additem").forEach(function (element) {
    element.addEventListener("click", function (e) {
      addItem(element.dataset.bsItem);
    });
  });
};

let builPagination = (data, page) => {
  let pageitems = "";
  if (data.previous === null) {
    pageitems += ` <li class="page-item disabled"><a class="page-link">Anterior</a></li>`;
  } else {
    pageitems += ` <li class="page-item "><a class="page-link" data-bs-target="${
      data.previous
    }" data-bs-page="${parseInt(page) - 1}">Anterior</a></li>`;
  }

  for (var i = 1; i < Math.ceil(data.count / 4) + 1; i++) {
    if (parseInt(page) === i) {
      pageitems += `<li class="page-item active"><a class="page-link" data-bs-target="/api/products/?format=json&page=${i}" data-bs-page="${i}">${i}</a></li>`;
    } else {
      pageitems += `<li class="page-item"><a class="page-link" data-bs-target="/api/products/?format=json&page=${i}" data-bs-page="${i}">${i}</a></li>`;
    }
  }

  if (data.next === null) {
    pageitems += ` <li class="page-item disabled"><a class="page-link">Siguiente</a></li>`;
  } else {
    pageitems += ` <li class="page-item "><a class="page-link" data-bs-target="${
      data.next
    }" data-bs-page="${parseInt(page) + 1}">Siguiente</a></li>`;
  }
  let target = document.getElementById("pageitems");
  target.innerHTML = pageitems;

  document.querySelectorAll(".page-link").forEach(function (element) {
    element.addEventListener("click", function (e) {
      loadProducts(element.dataset.bsTarget, element.dataset.bsPage);
    });
  });
};

let loadProducts = (url, page) => {
  fetch(url)
    .then((resp) => resp.json())
    .then(function (data) {
      builPagination(data, page);
      buildProductList(data.results);
    })
    .catch(function (error) {
      console.log(error);
    });
};

loadProducts("/api/products/?format=json&page=1", "1");

/* payment */
let saveItems = (orderid) => {
  for (var i = 0; i < orderItems.length; i++) {
    data = {
      order: orderid,
      product: orderItems[i].id,
      quantity: orderItems[i].units,
    };
    fetch("/api/orderproduct/", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "X-CSRFToken": csrftoken,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .catch((error) => console.error("Error:", error))
      .then((response) => {
        //console.log("Success:", response.id);
      });
  }
  location.reload();
};

let doPayment = (payment) => {
  data = {
    total: total,
    payment: payment,
    fullpayment: payment >= total,
    user: userid,
  };

  fetch("/api/orders/", {
    method: "POST",
    body: JSON.stringify(data),
    headers: { "X-CSRFToken": csrftoken, "Content-Type": "application/json" },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {
      saveItems(response.id);
    });
};

async function saveOrder() {
  document.getElementById("saveorder").setAttribute("disabled", true);
  await doPayment(0);
}
async function pay() {
  document.getElementById("pay").setAttribute("disabled", true);
  await doPayment(total);
}
async function partialpayment() {
  document.getElementById("partialpayment").setAttribute("disabled", true);
  let value = document.getElementById("partialpaymentbox").value;
  await doPayment(parseInt(value));
}

/* payment */
