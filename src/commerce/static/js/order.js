let csrftoken = document.cookie.replace(
  /(?:(?:^|.*;\s*)csrftoken\s*\=\s*([^;]*).*$)|^.*$/,
  "$1"
);
let orders = [];

let orderItem = (order) => {
  let item = `
        <div class="row align-items-start shadow p-3 mb-2 bg-body rounded">
            <div class="col">
                <p><strong>ID:</strong> ${order.id}</p>
                <p><strong>Fecha:</strong> ${order.updated}</p>
            </div>
            <div class="col">
                <p><strong>Precio Total:</strong> $ ${order.total}</p>
                <p><strong>Pago Parcial:</strong> $ ${order.payment}</p>
            </div>
    `;
  if (order.fullpayment) {
    item += `</div>`;
  } else {
    item += `
        <div class="col">
                <div class="d-flex justify-content-center">
                    <input type="text" class="" placeholder="Pago parcial" aria-label="" id="partialpaymentbox"
                        aria-describedby="button-addon2">
                    <button class="btn btn-secondary" type="button" id="partialpayment"
                        onclick="partialpayment(${order.id},${order.total},${order.payment})">Pago Parcial</button>
                </div>
            </div>
            <div class="col">
                <div class="d-flex justify-content-center mt-2">
                    <button type="button" class="btn btn-primary btn-lg" id="pay" 
                        onclick="pay(${order.id},${order.total})" >Pagar</button>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="${order.id}" >
                    <label class="form-check-label" for="" >
                        Seleccionar para pago
                    </label>
                </div>
            </div>
        </div>
        `;
  }
  return item;
};

let buildOrderList = (orders) => {
  let orderList = ``;
  for (var i = 0; i < orders.length; i++) {
    orderList += orderItem(orders[i]);
  }
  if (orderList.length > 1) {
    orderList += `
      <div class="d-flex justify-content-center m-5">
        <button type="button" class="btn btn-primary btn-lg" id="payorders" onclick="payorders()">Pagar Ordenes Seleccionadas</button>
      </div>
       `;
    let target = document.getElementById("orders");
    target.innerHTML = orderList;
  }
};

let loadOrders = (url) => {
  fetch(url)
    .then((resp) => resp.json())
    .then(function (data) {
      orders = data;
      buildOrderList(data);
    })
    .catch(function (error) {
      console.log(error);
    });
};

loadOrders("/api/ordersbyuser/");

/* payment */
let saveItems = (orderid) => {
  for (var i = 0; i < orderItems.length; i++) {
    data = {
      order: orderid,
      product: orderItems[i].id,
      quantity: orderItems[i].units,
    };
    fetch("/api/orderproduct/", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "X-CSRFToken": csrftoken,
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .catch((error) => console.error("Error:", error))
      .then((response) => {
        //console.log("Success:", response.id);
      });
  }
  location.reload();
};

let doPayment = (id, total, payment) => {
  data = {
    id: id,
    total: total,
    payment: payment,
    fullpayment: payment >= total,
    user: userid,
  };

  fetch(`/api/orders/${id}/`, {
    method: "PUT",
    body: JSON.stringify(data),
    headers: { "X-CSRFToken": csrftoken, "Content-Type": "application/json" },
  })
    .then((res) => res.json())
    .catch((error) => console.error("Error:", error))
    .then((response) => {});
};

async function pay(id, total) {
  await doPayment(id, total, total);
  location.reload();
}
async function partialpayment(id, total, payment) {
  let value = document.getElementById("partialpaymentbox").value;
  await doPayment(id, total, parseInt(value) + parseInt(payment));
  location.reload();
}
async function payorders() {
  document.querySelectorAll(".form-check-input").forEach(function (element) {
    for (var i = 0; i < orders.length; i++) {
      if (orders[i].id === parseInt(element.value) && element.checked) {
        doPayment(orders[i].id, orders[i].total, orders[i].total);
      }
    }
    location.reload();
  });
}

/* payment */
