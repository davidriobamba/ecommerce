let orderItem = (order) => {
  let item = `
        <div class="row align-items-start shadow p-3 mb-2 bg-body rounded">
            <div class="col">
                <p><strong>ID:</strong> ${order.id}</p>
                <p><strong>Fecha:</strong> ${order.updated}</p>
            </div>
            <div class="col">
                <p><strong>Precio Total:</strong> $ ${order.total}</p>
                <p><strong>Pago Parcial:</strong> $ ${order.payment}</p>
            </div>
        </div>
    `;

  return item;
};

let buildOrderList = (orders) => {
  let orderList = ``;
  for (var i = 0; i < orders.length; i++) {
    orderList += orderItem(orders[i]);
  }
  if (orderList.length > 1) {
    let target = document.getElementById("orders");
    target.innerHTML = orderList;
  }
};

let builPagination = (data, page) => {
  let pageitems = "";
  if (data.previous === null) {
    pageitems += ` <li class="page-item disabled"><a class="page-link">Anterior</a></li>`;
  } else {
    pageitems += ` <li class="page-item "><a class="page-link" data-bs-target="${
      data.previous
    }" data-bs-page="${parseInt(page) - 1}">Anterior</a></li>`;
  }

  for (var i = 1; i < Math.ceil(data.count / 4) + 1; i++) {
    if (parseInt(page) === i) {
      pageitems += `<li class="page-item active"><a class="page-link" data-bs-target="/api/paidorder/?page=${i}" data-bs-page="${i}">${i}</a></li>`;
    } else {
      pageitems += `<li class="page-item"><a class="page-link" data-bs-target="/api/paidorder/?page=${i}" data-bs-page="${i}">${i}</a></li>`;
    }
  }

  if (data.next === null) {
    pageitems += ` <li class="page-item disabled"><a class="page-link">Siguiente</a></li>`;
  } else {
    pageitems += ` <li class="page-item "><a class="page-link" data-bs-target="${
      data.next
    }" data-bs-page="${parseInt(page) + 1}">Siguiente</a></li>`;
  }
  let target = document.getElementById("pageitems");
  target.innerHTML = pageitems;

  document.querySelectorAll(".page-link").forEach(function (element) {
    element.addEventListener("click", function (e) {
      loadOrders(element.dataset.bsTarget, element.dataset.bsPage);
    });
  });
};

let loadOrders = (url, page) => {
  fetch(url)
    .then((resp) => resp.json())
    .then(function (data) {
      builPagination(data, page);
      buildOrderList(data.results);
    })
    .catch(function (error) {
      console.log(error);
    });
};

loadOrders("/api/paidorder/?page=1", "1");
