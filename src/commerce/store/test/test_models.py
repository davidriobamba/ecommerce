
from django.contrib.auth.models import User
from django.test import TestCase
from ..models import Product, Order, OrderProduct


class ModelsTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user(
            username='testuser', password='12345')
        product = Product.objects.create(name="Test Product", description="Test description",
                                         image="https://test.image.co", price=1000, quantity=100)
        order = Order.objects.create(user=user, total=5000,
                                     payment=2000, fullpayment=0)
        OrderProduct.objects.create(order=order, product=product, quantity=10)

    def test_product_exists(self):
        product = Product.objects.get(name="Test Product")
        self.assertEqual(product.name, 'Test Product')

    def test_order_exists(self):
        order = Order.objects.get(pk=1)
        self.assertIsNotNone(order)
        self.assertEqual(order.payment, 2000)

    def test_order_product_exists(self):
        orderproduct = OrderProduct.objects.get(pk=1)
        self.assertIsNotNone(orderproduct)
