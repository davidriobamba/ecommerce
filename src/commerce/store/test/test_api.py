import json
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework.test import APITestCase


class APIEndPoinTestCase(APITestCase):

    def test_post_products(self):
        client = APIClient(enforce_csrf_checks=True)
        test_product = {
            "id": 1,
            "name": "test",
            "description": "test test",
            "image": "test.test.co",
            "price": 5000,
            "quantity": 50
        }

        response = client.post(
            '/api/products/',
            test_product,
            format='json'
        )

        result = json.loads(response.content)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('name', result)
        self.assertIn('description', result)
        self.assertIn('image', result)
        self.assertIn('price', result)
        self.assertIn('quantity', result)
        self.assertEqual(result, test_product)

    def test_get_products(self):
        client = APIClient(enforce_csrf_checks=True)
        test_product = {
            "id": 1,
            "name": "test",
            "description": "test test",
            "image": "test.test.co",
            "price": 5000,
            "quantity": 50
        }

        client.post(
            '/api/products/',
            test_product,
            format='json'
        )

        client = APIClient()
        response = client.get('/api/products/')
        result = json.loads(response.content)
        self.assertEqual(result['count'], 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_order_forbidden(self):
        client = APIClient()
        test_order = {
            "total": 1000,
            "payment": 0,
            "fullpayment": False,
            "user": 1
        }

        response = client.post(
            '/api/orders/',
            test_order,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_order_product_forbidden(self):
        client = APIClient()
        test_order = {
            "order": 1,
            "product": 1,
            "quantity": 10
        }

        response = client.post(
            '/api/orderproduct/',
            test_order,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
