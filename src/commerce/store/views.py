from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required


def index(request):
    return render(request, 'store/store.html')


@login_required
def orders(request):
    return render(request, 'store/orders.html')


@login_required
def report(request):
    return render(request, 'store/report.html')
