from django.urls import path, include
from rest_framework import routers

from . import views
from . import api

app_name = 'store'

# Routers provide a way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'products', api.ProductViewSet)
router.register(r'orderproduct', api.OrderProductViewSet)
router.register(r'orders', api.OrderViewSet)
router.register(r'ordersbyuser', api.OrderByUserViewSet)
router.register(r'productsbyorder', api.ProductByOrderViewSet)
router.register(r'paidorder', api.PaidOrderViewSet)


urlpatterns = [
    path('api/', include(router.urls)),
    path('', views.index, name='index'),
    path('myorders', views.orders, name='myorders'),
    path('report', views.report, name='report'),
]
