"""
    Api module with serializers and viewsets for models
"""
from typing import Protocol
from rest_framework import serializers, viewsets
from rest_framework.pagination import PageNumberPagination
from .models import Product, Order, OrderProduct
from rest_framework.permissions import IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
from rest_framework import permissions


class CustomResultsSetPagination(PageNumberPagination):
    page_size = 4


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'image', 'price', 'quantity')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'updated', 'total',  'payment', 'fullpayment', 'user')


class OrderListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'updated', 'total',  'payment', 'fullpayment', 'user')


class OrderProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderProduct
        fields = ('id', 'order', 'product', 'quantity')


# ViewSets define the view behavior.


class ProductViewSet(viewsets.ModelViewSet):
    pagination_class = CustomResultsSetPagination
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_queryset(self):
        query_set = Product.objects.exclude(quantity=0).order_by('id')
        return query_set


class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class OrderByUserViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_queryset(self):
        query_set = Order.objects.filter(user=self.request.user.id)
        return query_set


class ProductByOrderViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductSerializer

    def get_queryset(self):
        query_set = OrderProduct.objects.filter(
            order_id=self.request.query_params.get('orderid', None))
        return query_set


class PaidOrderViewSet(viewsets.ModelViewSet):
    pagination_class = CustomResultsSetPagination
    permission_classes = [IsAdminUser]
    queryset = Order.objects.all()
    serializer_class = OrderListSerializer

    def get_queryset(self):
        query_set = Order.objects.exclude(fullpayment=0).order_by('id')
        return query_set


class OrderProductViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = OrderProduct.objects.all()
    serializer_class = OrderProductSerializer
