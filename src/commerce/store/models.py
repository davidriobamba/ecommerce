from django.db import models
from django.contrib.auth.models import User
from django.db.models import signals


class Product(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    image = models.TextField()
    price = models.IntegerField()
    quantity = models.IntegerField()


class Order(models.Model):
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now_add=True, blank=False)
    total = models.IntegerField()
    payment = models.IntegerField()
    fullpayment = models.BooleanField()


class OrderProduct(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()


def save_product(instance, update_fields, **kwargs):
    prod = Product.objects.get(pk=instance.product_id)
    prod.quantity = prod.quantity-instance.quantity
    prod.save()


signals.pre_save.connect(save_product, OrderProduct)
