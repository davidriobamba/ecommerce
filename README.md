# Ecommerce

## Getting started

E-commerce developed using the Django version 3.2.9 framework, it has web services built from Django Rest Framework version 3.12.4.

Features:

- A user can purchase products as part of an order.
- A payment can apply to one or more orders and an order can be paid by one or more payments.
- Provide a SQL report showing the paid orders info.

![image](/uploads/c8989dc940bf20ae76f128f80e83104b/image.png)

## Deploy - Docker

```
git clone https://gitlab.com/davidriobamba/ecommerce.git
cd ecommerce/dist
docker-compose up -d

start browser on http://localhost:8000/
```

## Deploy - Virtual Environment

```
git clone https://gitlab.com/davidriobamba/ecommerce.git
cd ecommerce/src/commerce
pip install -r requirements.py
python manage.py runserver

start browser on http://localhost:8000/
```

## Default Users

```
- Admin User
username: admin
password: admin

- Ordinary User
username: test
password: test

```
